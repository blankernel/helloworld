package ornek.uygulama;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;

public class OrnekActivity extends Activity {
	@Override
	public void onCreate(Bundle b){
		super.onCreate(b);

		// yeni metin kutusu oluştur
		TextView text = new TextView(this);

		// metin kutusuna "Merhaba BLANKERNEL!" yazdır
		text.setText("Merhaba BLANKERNEL!");

		// bu metin kutusunun uygulamanın 
		// içinde görünmesini sağla
		setContentView(text);

		// metin kutusu ekranı kaplayacak
		text.setLayoutParams(new LayoutParams(
				LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT
			)
		);

		// metin kutusu içindeki metin ortalanacak
		text.setGravity(Gravity.CENTER);
	}
}
